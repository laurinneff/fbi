#!/usr/bin/python3
import sys
import os
from PIL import Image
import argparse
from bitfield import make_bf, c_uint, c_uint64
from PIL import Image

fbi1head = make_bf('fbi1head', [
    ('ch1', c_uint, 2),
    ('ch2', c_uint, 2),
    ('ch3', c_uint, 2),
    ('ch4', c_uint, 2),
    ('ch1_bitd', c_uint, 4),
    ('ch2_bitd', c_uint, 4),
    ('ch3_bitd', c_uint, 4),
    ('ch4_bitd', c_uint, 4),
    ('width', c_uint, 16),
], c_uint64)

parser = argparse.ArgumentParser()
parser.add_argument(
    'source', help='the source file, must be an FBI file')
parser.add_argument(
    'target', help='the target file, any image type Pillow supports')
parser.add_argument(
    '--mode', help='the mode the new file should be written with', default=None)
args = parser.parse_args()

with open(args.source, 'rb') as source:
    if (source.read(3) != b'fbi'):
        print('Error: Not an FBI file')
        sys.exit(1)
    version = int(source.read(1))
    buf = None
    if version == 1:
        buf = fbi1head()

    if buf == None:
        print('Error: Unknown FBI version:', version)
        sys.exit(1)

    source.readinto(buf)  # populates buf with header data

    width = buf.width
    height = (os.path.getsize(args.source) - 9) // 4 // buf.width
    target = Image.frombytes('RGBA', (width, height),
                             source.read(os.path.getsize(args.source) - 9), decoder_name='raw')  # Remove 9 bytes to only have image data

    target.convert(args.mode if args.mode !=
                   None else target.mode).save(args.target)  # if the user set a mode, we should use it, otherwise the default of RGBA is used
