#!/usr/bin/python3
from sys import stdout
import argparse
from bitfield import make_bf, c_uint, c_uint64
from PIL import Image
import struct

fbi1head = make_bf('fbi1head', [
    ('ch1', c_uint, 2),
    ('ch2', c_uint, 2),
    ('ch3', c_uint, 2),
    ('ch4', c_uint, 2),
    ('ch1_bitd', c_uint, 4),
    ('ch2_bitd', c_uint, 4),
    ('ch3_bitd', c_uint, 4),
    ('ch4_bitd', c_uint, 4),
    ('width', c_uint, 16),
], c_uint64)

parser = argparse.ArgumentParser()
parser.add_argument(
    'source', help='the source file, any image type Pillow supports')
parser.add_argument(
    'target', help='the target file, will use the latest FBI version')
args = parser.parse_args()

# convert the image to rgba as this script can't write in other modes yet
source = Image.open(args.source).convert('RGBA')
width = source.width
height = source.height

header = fbi1head()
header.ch1 = 0  # assume rgba
header.ch2 = 1
header.ch3 = 2
header.ch4 = 3
header.ch1_bitd = 8  # assume 8 bits per channel
header.ch2_bitd = 8
header.ch3_bitd = 8
header.ch4_bitd = 8
header.width = source.width


with open(args.target, 'wb') as target:
    target.write(b'fbi1')  # version and header
    target.write(header)
    for y in range(height):
        for x in range(width):
            # progress text
            stdout.write(f'\r{(x+y*width)/(width*height)*100:.3f}%')
            pixel = source.getpixel((x, y))
            target.write(struct.pack(
                'B B B B', pixel[0], pixel[1], pixel[2], pixel[3]))

print()
