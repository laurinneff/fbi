#!/usr/bin/python3
import sys
import os
from bitfield import make_bf, c_uint, c_uint64

fbi1head = make_bf('fbi1head', [
    ('ch1', c_uint, 2),
    ('ch2', c_uint, 2),
    ('ch3', c_uint, 2),
    ('ch4', c_uint, 2),
    ('ch1_bitd', c_uint, 4),
    ('ch2_bitd', c_uint, 4),
    ('ch3_bitd', c_uint, 4),
    ('ch4_bitd', c_uint, 4),
    ('width', c_uint, 16),
], c_uint64)

with open(sys.argv[1], 'rb') as file:
    if (file.read(3) != b'fbi'):
        print('Error: Not an FBI file')
        sys.exit(1)
    version = int(file.read(1))
    buf = None
    if version == 1:
        buf = fbi1head()

    if buf == None:
        print('Error: Unknown FBI version:', version)
        sys.exit(1)

    file.readinto(buf)  # Reads the header data into the buf variable

    print('fbi v' + str(version))
    print('width:', buf.width)
    # Remove 9 bytes to only have image data, and divide by 4 because 1 pixel is 4 bytes.
    print('height:', (os.path.getsize(sys.argv[1]) - 9) // 4 // buf.width)
