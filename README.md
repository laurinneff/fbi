# fbi

Simple image format storing files as framebuffer without compression

# Usage

    ./fbiinfo.py file
Get info about file

    ./conv2fbi.py source target
Convert source to a 4x8bit RGBA FBI file and store it as target

    ./fbi2other.py source target [--mode MODE]
Convert source (4x8bit RGBA FBI file) to another format and store it as target. Optionally, you can specify the new mode with --mode (required when converting to jpeg, as that doesn't support rgba)

# TODO

Add support for other channel combinations than 4x8bit RGBA