# fbi headers

store width and bit order/depth of the image file

example header (binary):  
01100110 01100010 01101001 00110001  00 01 10 11 1000 1000 1000 1000 0000000000100000  
fbi version 1, rgba, 32 bit colors (8 each), 32 pixels wide

## channels:
00 - red  
01 - green  
10 - blue  
11 - alpha

## header fields:
8 bits: channel colors, 2 bits each, 4 channels in total

16 bits: bit depth of each channel, 4 bits each

16 bits: width of image

total length: 40 bits/5 bytes
